# Instruction on How to Start and Contribute

### 1. Clone the Project
   First of all, clone this project on to your local directories.  
   You could do it by running `git clone https://gitlab.com/naufalauddin/ta-avr-project`
   on your console.  
   
   Make sure the console is on the directories where you wanted the project will be.

### 2. Move your directory to cloned repo
   You could do it by running command below on the console.
   ```
   cd ta-avr-project
   ```

### 3. Open AVR Studio and Create a New Project
  Open AVR Studio and choose create new project  

  ![create new project](./misc/create-new-project.png)  

  Choose Atmel AVR Assembler and make sure create initial file and create folder option
  checked off. You could choose any name for the project. **This is a crutial step, so
  make sure to do it properly.**  

  ![project option](./misc/project-configuration.png)

  Choose atmega8515 as the base microchip used in the project.  

  ![atmega config](./misc/atmega-option.png)

### 4. Set main-program.asm as An Entry File in AVR Project
  In the currently opened project, on the projects tab (the project tab could be find
  on the right side), right click on Source Files and choose add files to project.  

  ![add files to project](./misc/add-files-to-project.png)

  Locate ta-avr-project directory, then choose main_program.asm and open file.  

  ![open main_program.asm](./misc/open-main-program-asm.png)

### 5. Start coding
  ![Your project is set, now start coding](./misc/start-code.png)

  Now that the project is set. You could start coding. And don't forget to commit every
  new feature you implemented. In commiting git goes the say 'more the merrier'. Just
  make sure your commit is clean.

  ***Please Remember to use a meaningful comment for commit***
