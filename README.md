# Fundamental of Computer Organization's Final Project

## Team
* Manuel Yoseph Ray
* Naufal Alauddin Hilmi
* Tsaqif naufal

## About
The final project is a game of red-lights green-lights played on an atmega8515
simulated on Hapsim and AVR Studio. The main objective is for the player to move
when the green lights turned on and stop when red lights turned on. Player has
to move during green lights. If the player didn't move during this interval, or
moved when the red lights is turned on, the game will be terminated and the game
will be restarted. System will save the score scored in the game, and player
could try again to beat the score.

## Note for Contributor
For further instruction on how to start a contribution, read [this](./note.md)  
ps: Only for the team
