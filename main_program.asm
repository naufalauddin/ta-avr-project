;======================================================================
; An Atmega Project Presented by _________
;
;	This is a game of Red Light Green Light, where player are ought to
;	move when the green light turns on, and stop when red light turns
;	on. Sleeping (hasn't moved during interval) on greenlights is
;	prohibited.
;
;	System will calculate the score based on how far the player has
; 	moved. Player ought to try as much as he can to beat the highest
;	score.
;
;	Happy playing.
;======================================================================

;======================================================================
; PROCESSOR	: ATMEGA8515
; COMPLIER	: AVRASM
;======================================================================

;======================================================================
; DEFINITIONS
;======================================================================

.include "m8515def.inc"	; atmega8515 defined
.def temp = r16			; temporary register
.def player_state = r17	; run/stop state register 0:=stop 1:=run
.def light_state  = r18	; lights green/red state, 0:=red 1:=green
.def timer_count = r19	; timer1 intterupt counter
.def comp_timer = r20	; timer1 interrupt count compare

;======================================================================
; RESETS and INTERRUPT VESCTORS
;======================================================================

.org $00
rjmp MAIN
.org $01			; Button interrupt
rjmp RUN_STOP
.org $02
rjmp INT_1
.org $04			; Timer1 interrupt
rjmp COMP_TIME_INT	

;======================================================================
; CODE SEGMENTS
;======================================================================

MAIN:

INIT_STACK:
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

INIT_INTERRUPT:
	
	ldi temp, 0b01000000
	out GICR, temp			;set external interrupt 1
	ldi temp, 0b00000010
	out MCUCR, temp			;set interrupt on falling edge
	clr temp
	out GIFR, temp

	; timer0 will be the reference on random number generation
	clr temp
	out TIMSK, temp
	ldi temp, 1<<CS00		; set to timer0 equals to PK
	out TCCR0, temp

	; timer 1 will be the one used to set lights red or green

	ldi temp, 3
	out TCCR1A, temp		; set prescaler for timer 1 to 256
	out TCCR1B, temp

	ldi temp, 1<<OCIE1A
	out TIMSK, temp			; enable Timer/Counter1 compare  interrupt

	ldi temp, 0x84			; compare with 15625, or 0.5s in presabler 256
	out OCR1AL, temp
	ldi temp, 0x1e
	out OCR1AH, temp

	ldi comp_timer, 4

INIT_LED:

	clr temp
	out PORTD, temp
	ldi temp, 0xF0
	out PORTB, temp

INIT_LCD:

SPLASH_SCREEN:

rjmp INIT_START_GAME

	WAIT:
	rjmp wait

INIT_START_GAME:
	
	clr temp
	out TCNT1L, temp
	out TCNT1H, temp
	sei						;Set global interrupt


PLAY:
	nop
	nop
	rjmp PLAY

RUN_STOP:
	
	push temp
	in temp, SREG
	push temp
	

	ori player_state, 0x80

	tst player_state
	breq MAKE_RUN

	ldi player_state, 0
	pop temp
	in temp, SREG
	pop temp
	reti

	MAKE_RUN:

	tst light_state
	breq GAME_OVER_CLEAR_INT_STACK

	ldi player_state, 1
	pop temp
	in temp, SREG
	pop temp
	reti


; light change

COMP_TIME_INT:

	push temp
	in temp, SREG
	push temp

	inc timer_count

	cp timer_count, comp_timer
	brne RESET_TIMER

	SWITCH_LIGHTS:

		in temp, PORTB
		com temp
		out PORTB, temp

		ldi temp, 1
		eor light_state, temp	; switch light state

	CHECK_HAS_RUN:

		mov temp, player_state
		andi temp, 0x80

		tst temp
		breq GAME_OVER_CLEAR_INT_STACK


	;Set compare register of timer1 to a random number
	SET_RND_COMPARE:

		in temp, TCNT0
		andi temp, 0b00001111
		subi temp, -3
		mov comp_timer, temp


	RESET_TIMER_COUNT:
		
		clr timer_count

	RESET_TIMER:

		clr temp
		out TCNT1H, temp
		out TCNT1L, temp

	pop temp
	out SREG, temp
	pop temp
	
	reti


GAME_OVER_CLEAR_INT_STACK:

	pop temp	; pop SREG
	pop temp	; pop temp
	pop temp	; pop ret address
	pop temp	; pop ret address

GAME_OVER:
	
	OFF_TIMER_INTERRUPT:
	clr temp
	out TCCR1A, temp
	out TCCR1B, temp		
	out TIMSK, temp	
	out OCR1AL, temp
	out TIFR, temp	

	ldi temp, 0xFF
	out PORTA, temp

DEAD:

	rjmp DEAD

INT_1:
	reti
